<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lapormain extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("user/Lapor_main", "lm");
		$this->load->library("response_message");	
	}

	public function index(){
		$this->load->view('user/insert_lapor');
	}

	public function val_insert(){
		$config_val_input = array(
            array(
                'field'=>'negara',
                'label'=>'Negara Asal',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'prov',
                'label'=>'Provinsi Asal',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'kota',
                'label'=>'Kota Asal',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'kec',
                'label'=>'Kecamatan Asal',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'kel',
                'label'=>'Kelurahan Asal',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'nama',
                'label'=>'Nama Pelapor',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'nik',
                'label'=>'Nomor Identitas',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'tmp_lhr',
                'label'=>'Tempat Lahir',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'tgl_lhr',
                'label'=>'Tanggal Lahir',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'jk',
                'label'=>'Jenis Kelamin',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'pekerjaan',
                'label'=>'Pekerjaan',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'kwn',
                'label'=>'Kewarganegaraan',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'alamat',
                'label'=>'Alamat Pelapor',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'tlp',
                'label'=>'Telepon Terlapor',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'fax',
                'label'=>'Fax Pelapor',
                'rules'=>'numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'email',
                'label'=>'Email Pelapor',
                'rules'=>'required|valid_emails',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                 )
                       
            ),
            array(
                'field'=>'peristiwa',
                'label'=>'Peristiwa',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'tmp_kejadian',
                'label'=>'Tempat Kejadian',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'tgl_kejadian',
                'label'=>'Tanggal Kejadian',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'tgl_diketahui',
                'label'=>'Tanggal di Ketahui',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'nama_terlapor',
                'label'=>'Nama Terlapor',
                'rules'=>'required|alpha_numeric_spaces',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                 )
                       
            ),
            array(
                'field'=>'alamat_terlapor',
                'label'=>'Alamat Terlapor',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'tlp_terlapor',
                'label'=>'Tanggal Terlapor',
                'rules'=>'required|numeric',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                	'required'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                 )
                       
            ),
            array(
                'field'=>'saksi',
                'label'=>'Saksi Pelapor',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'bukti',
                'label'=>'Bukti Pelapor',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'uraian_kejadian',
                'label'=>'Uraian Kejadian',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            ),
            array(
                'field'=>'tmp_lapor',
                'label'=>'Tempat Pelaporan',
                'rules'=>'required',
                'errors'=>array(
                	'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                 )
                       
            )
           
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();

	}

	public function insert(){
		if($this->val_insert()){
			$negara = $this->input->post("negara");
			$prov = $this->input->post("prov");
			$kota = $this->input->post("kota");
			$kec = $this->input->post("kec");
			$kel = $this->input->post("kel");
			$nama = $this->input->post("nama");
			$nik = $this->input->post("nik");
			$tmp_lhr = $this->input->post("tmp_lhr");
			$tgl_lhr = $this->input->post("tgl_lhr");
			$jk = $this->input->post("jk");
			$pekerjaan = $this->input->post("pekerjaan");
			$kwn = $this->input->post("kwn");
			$alamat = $this->input->post("alamat");
			$tlp = $this->input->post("tlp");
			$fax = $this->input->post("fax");
			$email = $this->input->post("email");
			$peristiwa = $this->input->post("peristiwa");
			$tmp_kejadian = $this->input->post("tmp_kejadian");
			$tgl_kejadian = $this->input->post("tgl_kejadian");
			$tgl_diketahui = $this->input->post("tgl_diketahui");
			$nama_terlapor = $this->input->post("nama_terlapor");
			$alamat_terlapor = $this->input->post("alamat_terlapor");
			$tlp_terlapor = $this->input->post("tlp_terlapor");
			$saksi = $this->input->post("saksi");
			$bukti = $this->input->post("bukti");
			$uraian_kejadian = $this->input->post("uraian_kejadian");
			$tmp_lapor = $this->input->post("tmp_lapor");
			$bukti_foto = $this->input->post("bukti_foto");

			$data_insert = array(
							"negara"=>$negara,
							"prov"=>$prov,
							"kota"=>$kota,
							"kec"=>$kec,
							"kel"=>$kel,
							"nama"=>$nama,
							"nik"=>$nik,
							"tmp_lhr"=>$tmp_lhr,
							"tgl_lhr"=>$tgl_lhr,
							"jk"=>$jk,
							"pekerjaan"=>$pekerjaan,
							"kwn"=>$kwn,
							"alamat"=>$alamat,
							"tlp"=>$tlp,
							"fax"=>$fax,
							"email"=>$email,
							"peristiwa"=>$peristiwa,
							"tmp_kejadian"=>$tmp_kejadian,
							"tgl_kejadian"=>$tgl_kejadian,
							"tgl_diketahui"=>$tgl_diketahui,
							"nama_terlapor"=>$nama_terlapor,
							"alamat_terlapor"=>$alamat_terlapor,
							"tlp_terlapor"=>$tmp_kejadian,
							"saksi"=>$saksi,
							"bukti"=>$bukti,
							"uraian_kejadian"=>$uraian_kejadian,
							"tmp_kejadian"=>$tmp_kejadian,
							"waktu_lapor"=>date("Y-m-d h:i:s"),
							"bukti_gmbr"=>"surya.jpg"
						);
			if($this->lm->insert($data_insert)){
				print_r($this->response_message->get_success_msg("PENDAFTARAN_SUC"));
			}else{
				print_r($this->response_message->get_error_msg("PENDAFTARAN_FAIL"));
			}
			
		}else{
			print_r(validation_errors());
		}
	}


	public function delete(){

	}
}
