<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php print_r(form_open_multipart("user/lapormain/insert"));?>
		negara : <input type="text" name="negara" id="negara"><br>
		prov : <input type="text" name="prov" id="prov"><br>
		kota : <input type="text" name="kota" id="kota"><br>
		kec : <input type="text" name="kec" id="kec"><br>
		kel : <input type="text" name="kel" id="kel"><br>
		nama : <input type="text" name="nama" id="nama"><br>
		nik : <input type="number" name="nik" id="nik"><br>
		tmp_lhr : <input type="text" name="tmp_lhr" id="tmp_lhr"><br>
		tgl_lhr : <input type="date" name="tgl_lhr" id="tgl_lhr"><br>
		jk : <select name="jk" id="jk">
				<option value="0">Perempuan</option>
				<option value="1">Laki-Laki</option>
			</select><br>
		pekerjaan : <input type="text" name="pekerjaan" id="pekerjaan"><br>
		kwn : <select name="kwn" id="kwn">
				<option value="0">Warga Neagara Indonesia</option>
				<option value="1">Warga Neagara Asing</option>
			</select><br>
		alamat : <input type="text" name="alamat" id="alamat"><br>
		tlp : <input type="number" name="tlp" id="tlp"><br>
		fax : <input type="text" name="fax" id="fax"><br>
		email : <input type="email" name="email" id="email"><br>
		
		peristiwa : <input type="text" name="peristiwa" id="peristiwa"><br>
		tmp_kejadian : <input type="text" name="tmp_kejadian" id="tmp_kejadian"><br>
		tgl_kejadian : <input type="date" name="tgl_kejadian" id="tgl_kejadian"><br>
		tgl_diketahui : <input type="date" name="tgl_diketahui" id="tgl_diketahui"><br>
		nama_terlapor : <input type="text" name="nama_terlapor" id="nama_terlapor"><br>
		alamat_terlapor : <input type="text" name="alamat_terlapor" id="alamat_terlapor"><br>
		tlp_terlapor : <input type="number" name="tlp_terlapor" id="tlp_terlapor"><br>
		saksi : 			<table class="table" border="1">
                               <thead>
                                  <tr>
                                     <th scope="col" width="10%">No</th>
                                     <th scope="col" width="30%">Nama Saksi</th>
                                     <th scope="col" width="40%">Alamat</th>
                                     <th scope="col" width="20%">Telephone</th>
                                     <th scope="col" width="20%">Aksi</th>
                                  </tr>
                               </thead>
                               <tr>
                                     <th scope="row">#</th>
                                     <td>
                                        <input type="text" class="form-control" name="nama_saksi" id="nama_saksi" placeholder="Prof. Dr. Sukonto Legowo, S.ip., MBAH.,"/>
                                     </td>
                                     <td>
                                        <input type="text" class="form-control" name="alamat_saksi" id="alamat_saksi" placeholder="JL masjid"/>
                                     </td>
                                     <td>
                                        <input type="text" class="form-control" name="tlp_saksi" id="tlp_saksi" placeholder="081230xxxxxx"/>
                                     </td>
                                     <td>
                                        <button type="submit" id="add_saksi" name="add_saksi" class="btn btn-success">Tambah</button>
                                     </td>
                               </tr>
                               <tbody id="output_saksi">
                                  
                               </tbody>
                            </table><br>
		bukti : 			<table class="table" border="1" width="100%">
                               <thead>
                                  <tr>
                                     <th scope="col" width="10%">No</th>
                                     <th scope="col" width="*">Bukti</th>
                                     <th scope="col" width="20%">Aksi</th>
                                  </tr>
                               </thead>
                               <tr>
                                     <th scope="row">#</th>
                                     <td>
                                     	<textarea class="form-control" name="bukti" id="bukti" placeholder="bukti terjaring rahazia di kota malang" style="width: 97%; height: 70px;"></textarea>
                                     </td>
                                     
                                     <td>
                                        <button type="submit" id="add_bukti" name="add_bukti" class="btn btn-success">Tambah</button>
                                     </td>
                               </tr>
                               <tbody id="output_saksi">
                                  
                               </tbody>
                            </table><br><br>
		uraian_kejadian : <input type="text" name="uraian_kejadian" id="uraian_kejadian"><br>
		tmp_lapor : <input type="text" name="tmp_lapor" id="tmp_lapor"><br>

		bukti_foto : <input type="file" name="bukti_foto" id="bukti_foto"><br>

		<button type="submit" class="ok" id="ok" name="ok">Kirim Laporan</button>
	</form>

	<script type="text/javascript" src="<?php base_url()."assets/js/jquery-3.2.1.js";?>"></script>
	<script type="text/javascript">
		$(document).ready(function(){

		});

					var list_saksi = [];
                    
                    function add_saksi_f(){
                        var val_saksi = $("#saksi").val();
                        if(list_saksi.length < 2){
                            if(val_saksi != ""){
                                list_saksi.push(val_saksi);
                                $("#saksi").val("");   
                            }
                        }
                        console.log(list_saksi); 
                        show_saksi();
                    }
                    
                    $("#add_saksi").click(function(){
                        add_saksi_f();
                        //sweet();
                    });
                    
                    $("#add_saksi").keypress(function(e){
                        if(e.keyCode == 13){
                            add_saksi_f();
                        }
                    });
                    
                    $("#saksi").keypress(function(e){
                        if(e.keyCode == 13){
                            add_saksi_f();
                        }
                    });
                    
                    function del_saksi(index_start){
                        list_saksi.splice(index_start,1);
                        console.log(list_saksi); 
                        show_saksi();
                    }
                    
                    function show_saksi(){
                        var content_saksi = "";
                        for(var i = 0; i<list_saksi.length; i++){
                            var no = i+1;
                            content_saksi += "<tr><td>"+no+
                            "</td><td>"+list_saksi[i]+
                            "</td><td align=\"center\"><a id=\"del_tmp\" onclick=\"del_saksi("+i+")\"><i class=\"text-danger\"><i class=\"fa fa-close\"></i></i></a></td></tr>";
                        }
                        $("#output_saksi").html(content_saksi);
                    }
	</script>
</body>
</html>