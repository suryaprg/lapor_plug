<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lapor_main extends CI_Model {

	public function get_user_all(){
		$data = $this->db->get("plug_lapor")->result();
		return $data;
	}

	public function get_user($where){
		$data = $this->db->get_where("plug_lapor", $where)->row_array();
		return $data;
	}

	public function insert($data){
		$data = $this->db->insert("plug_lapor",$data);
		return $data;
	}

	public function delete($where){
		$data = $this->db->delete("plug_lapor",$where);
		return $data;
	}

	public function update($data, $where){
		$data = $this->db->update("plug_lapor",$data,$where);
		return $data;
	}

}
